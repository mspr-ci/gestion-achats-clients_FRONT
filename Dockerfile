FROM node:lts-alpine

COPY . app/mspr-ci-front

WORKDIR app/mspr-ci-front

RUN npm install

EXPOSE 8080

CMD npm run serve