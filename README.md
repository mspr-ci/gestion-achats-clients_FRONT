# gestion-achats-clients_front
[![Coverage Status](https://coveralls.io/repos/gitlab/mspr-ci/gestion-achats-clients_FRONT/badge.svg)](https://coveralls.io/gitlab/mspr-ci/gestion-achats-clients_FRONT)
[![Depfu](https://badges.depfu.com/badges/7104e41e37969b2d3bb44ff067440df3/overview.svg)](https://depfu.com/gitlab/mspr-ci/gestion-achats-clients_FRONT?project_id=10637)
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
