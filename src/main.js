import "bootstrap/dist/css/bootstrap.min.css";
import "jquery/src/jquery.js";
import "popper.js/dist/popper.min.js";
import "bootstrap/dist/js/bootstrap.min.js";

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";
require("dotenv").config();

Vue.config.productionTip = false;

if (process.env.NODE_ENV === "production") {
  axios.defaults.baseURL = process.env.BASE_URL;
}

Vue.prototype.$http = axios;

new Vue({
  render: h => h(App),
  router,
  components: { App }
}).$mount("#app");
