import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/Home";
import CustomerList from "@/components/CustomerList";
import CustomerAdd from "@/components/CustomerAdd";
import ProductList from "@/components/ProductList";
import ProductAdd from "@/components/ProductAdd";
import OrderList from "@/components/OrderList";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
      props: { msg: "Liste des clients" }
    },
    {
      path: "/customers",
      name: "CustomerList",
      component: CustomerList,
      props: { msgCustomersList: "Liste des clients" }
    },
    {
      path: "/addCustomer",
      name: "CustomerAdd",
      component: CustomerAdd,
      props: { msg: "Ajout d'un nouveau client" }
    },
    {
      path: "/products",
      name: "ProductList",
      component: ProductList,
      props: { msgProductList: "Liste des produits" }
    },
    {
      path: "/addProduct",
      name: "ProductAdd",
      component: ProductAdd,
      props: { msgProduct: "Ajout d'un nouveau produit" }
    },
    {
      path: "/orders",
      name: "OrderList",
      component: OrderList,
      props: { msgOrderList: "Liste des commandes" }
    }
  ]
});
