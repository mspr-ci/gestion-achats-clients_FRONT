module.exports = {
  devServer: {
    disableHostCheck: true,
    proxy: {
      "^/customers": {
        target: "http://mspr-ci-back.caprover.mohamedhenni.wtf/",
        ws: true,
        changeOrigin: true
      },
      "^/products": {
        target: "http://mspr-ci-back.caprover.mohamedhenni.wtf/",
        ws: true,
        changeOrigin: true
      },
      "^/orders": {
        target: "http://mspr-ci-back.caprover.mohamedhenni.wtf/",
        ws: true,
        changeOrigin: true
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/assets/scss/_variables.scss";`
      }
    }
  }
};
